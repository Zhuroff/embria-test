const functions = {
  markCurrentChapter(i) {
    let asides = document.querySelectorAll('.aside_nav_item')
    asides.forEach(li => li.classList.remove('--active'))
    asides.item(i).classList.add('--active')
  }
}

export default functions