import Vue from 'vue'

const months_shortcuts = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

const app_filters = {
  formatCreatedDate(date) {
    date = new Date(date)

    let year = date.getFullYear()
		let month = months_shortcuts[date.getMonth()]
		let day = date.getDate()

		if (day < 10) {
			day = '0' + day
		}

		return `${year} ${month} ${day}`
  },

  formatUpdatedDate(date) {
    let now = Date.now()
    let when = new Date(date).getTime()
    let years = Math.trunc((now - when) / (1000 * 60 * 60 * 24) / 365)
    let days = Math.trunc((now - when) / (1000 * 60 * 60 * 24))
    let hours = Math.trunc((now - when) / (1000 * 60 * 60))
    let mins = Math.trunc((now - when) / (1000 * 60))

    if (years === 1) {
      return `${years} year ago`;
    } else if (years > 1) {
      return `${years} years ago`;
    } else if (days < 365 && hours > 42) {
      return `${days} days ago`;
    } else if (mins < 60) {
      return `${mins} minutes ago`;
    } else {
      return `${hours} hours ago`;
    }
  }
}

for (let name in app_filters) {
	Vue.filter(name, app_filters[name])
}

export default app_filters