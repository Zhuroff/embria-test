import Vue from 'vue'
import App from './App.vue'
import { Snackbar, Table, Toast } from 'buefy'
import VueAxios from 'vue-axios'
import axios from 'axios'
import router from './router'
import store from './store'
import 'simplebar/dist/simplebar.min.css'
import 'buefy/dist/buefy.css'

Vue.use(Snackbar)
Vue.use(Table)
Vue.use(Toast)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
