import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({

  actions: {
    updateLayoutState: (ctx, value) => {
      ctx.commit('setLayoutState', value)
    },

    storeTableContents: (ctx, data) => {
      ctx.commit('setTableContents', data)
    },

    storeHeadings: (ctx, data) => {
      ctx.commit('setHeadings', data)
    },

    storeDocument: (ctx, data) => {
      ctx.commit('pushDocument', data)
    },

    updateThemeStyle: (ctx, val) => {
      ctx.commit('setThemeStyle', val)
    },

    storeChangedDocument(ctx, data) {
      ctx.commit('updateDocument', data)
    }
  },

  mutations: {
    setLayoutState: (state, value) => {
      state.layout = value
    },

    setTableContents: (state, data) => {
      state.table_contents = data
    },

    setHeadings: (state, data) => {
      state.headings.push(data)
    },

    pushDocument: (state, data) => {
      state.documents.push(data)
    },

    setThemeStyle: (state, val) => {
      state.is_theme_dark = val
    },

    updateDocument(state, data) {
      state.documents
        .find(doc => doc.document_id === data.doc_id).document_content
        .find(part => part.part_id === data.part_id).content = data.content
    }
  },

  state: {
    table_contents: [],

    headings: [],

    documents: [],

    layout: true,

    is_theme_dark: false
  },

  getters: {
    getLayoutType: state => {
      return state.layout
    },

    getTableContents: state => {
      return state.table_contents
    },

    getSingleDocument: state => id => {
      return state.table_contents.find(doc => doc.id == id)
    },

    getTheme: state => {
      return state.is_theme_dark
    }
  }
})
