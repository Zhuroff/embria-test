# Embria-test

## Install Yarn (if it not installed)
```
npm i yarn -g
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run watch
```

### Compiles and minifies for production
```
yarn run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
